'use strict'
var transition = require('../lib/transition')

module.exports = (app, addon) => {
  // Root route. This route will serve the `atlassian-connect.json` unless the
  // documentation url inside `atlassian-connect.json` is set
  app.get('/', (req, res) => {
    res.format({
      // If the request content-type is text-html, it will decide which to serve up
      'text/html': () => {
        res.redirect('/atlassian-connect.json')
      },
      // This logic is here to make sure that the `atlassian-connect.json` is always
      // served up when requested by the host
      'application/json': () => {
        res.redirect('/atlassian-connect.json')
      }
    })
  })

  // Add any additional route handlers you need for views or REST resources here...
  app.get('/view', addon.authenticate(), (req, res) => {
    res.render('workflow/view', {
      id: req.param('id'),
      config: JSON.parse(req.param('config'))
    })
  })

  app.get('/edit', addon.authenticate(), (req, res) => {
    res.render('workflow/create', {
      id: req.param('id'),
      config: req.param('config')
    })
  })

  app.get('/create', (req, res) => {
    res.render('workflow/create', {})
  })

  app.post('/triggered', addon.authenticate(), (req, res) => {
    var issueId = req.body.issue.id
    var config = JSON.parse(req.body.configuration.value)
    transition(config, issueId, addon.httpClient(req), res)
      .then(result => res.send(result[0]).send({result: result[1]}))
  })

  // micros health check
  app.get('/healthcheck', (req, res) => res.send(200))

  // simple add-on stats
  app.get('/stats', (req, res) =>
    addon.settings.getAllClientInfos().then(clients => res.json({numTenants: (clients || []).length}), () => res.send(500)))

}
