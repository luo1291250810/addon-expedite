# Expedite add-on

This add-on provides a post function to re-transition an issue based on either:

* The group membership of the reporter
* The priority of the issue

## Privacy and information security
* This add-on looks up issue details, group membership and issue priority details, but stores *nothing*!
* JIRA stores the necessary configuration alongside workflow data.

## Caveats
* Unfortunately some gaps in the public REST API mean that the name of the transition has to be typed in without auto-complete.
* This plugin can't write to any fields or do anything else fancy during a transition, only trigger it.
* If you're adding this to the 'Create' transition, you should put it last (or at least after the re-index / event step),
 to ensure the issue is ready to be transitioned when this expedite kicks in
* It's possible to create a transition loop using this plugin. Be careful!

## Development
* The add-on is built using atlassian-connect-express, and deployed on micros with a postgres backing for client info.
* The add-on (besides the front-end bit) is written in ES 2015 (but only uses the features supported out of the box by `node --harmony`).
* Contributions are welcome!