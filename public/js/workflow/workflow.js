/* global AJS */
/* global AP */
/* global _ */

AP.require(['jira'], function (jira) {
  var DEFAULT_TRANSITION = 'Open'
  var $ = AJS.$

  jira.WorkflowConfiguration.onSave(function () {
    var config = {
      transitionName: $('#transition-name').val()
    }

    if (byGroup()) {
      config.group = { id: groupValue().id, name: groupValue().id }
    } else {
      var priority = priorityValue()
      config.priority = {
        id: priority.id,
        name: priority.text
      }
    }

    return JSON.stringify(config)
  })

  jira.WorkflowConfiguration.onSaveValidation(function () {
    var errors = []
    if (!$('#transition-name').val()) {
      errors.push('Please provide the name of a transition to execute')
    }
    if (byGroup()) {
      if (!(groupValue().id)) {
        errors.push('Please select a group to execute the transition for')
      }
    } else {
      if (!priorityValue().id) {
        errors.push('Please select a priority level to execute the transition for')
      }
    }

    validationErrors(errors)
    return !errors.length
  })

  function priorityValue () {
    var $priority = $('#priority-picker')
    return {id: $priority.val(), text: $priority.select2('data').text}
  }

  function groupValue () {
    var $group = $('#group-picker')
    return {id: $group.val(), text: $group.val()}
  }

  function byGroup () {
    return $('#postfunction-config').find('input[value=group]').attr('checked')
  }

  function validationErrors (errors) {
    $('#validation-errors').find('ul').empty().append($.map(errors, function (message) {
      return $('<li></li>').text(message)
    }))
  }

  function bindForm (config) {
    $('#transition-name').val(config.transitionName || DEFAULT_TRANSITION)
    if (config.group) {
      $('#group-picker').select2('data', {id: config.group.name, text: config.group.name})
    } else if (config.priority) {
      $('#priority').prop('checked', true).change()
      $('#priority-picker').select2('data', {id: config.priority.id, text: config.priority.name})
    }
  }

  $(function () {
    AP.require('request', function (request) {
      request({
        url: '/rest/api/2/priority',
        type: 'GET',
        success: function (data) {
          data = _.map(JSON.parse(data), function (item) {
            return {id: item.id, text: item.name}
          })
          $('#priority-picker').auiSelect2({
            data: data
          })
          setTimeout(function () {
            bindForm(JSON.parse($('#postfunction-config').attr('data-config') || '{}'))
          }, 100)
        }
      })
    })

    $('#group-picker').auiSelect2({
      ajax: {
        url: '/rest/api/2/groups/picker',
        data: function (term) {
          return {
            query: term
          }
        },
        cache: 'true',
        results: function (data) {
          var results = _.map(JSON.parse(data).groups, function (item) {
            return {id: item.name, text: item.name}
          })
          return {results: results}
        },
        transport: function (params) {
          var aborted = false
          AP.require('request', function (request) {
            request({
              url: params.url,
              type: params.type,
              data: params.data,
              success: function (a) {
                if (!aborted) {
                  params.success(a)
                }
              }
            })
          })
          return {
            abort: function () {
              aborted = true
            }
          }
        }
      }
    })
  })

})
