'use strict'
const DEFAULT_TIMEOUT_MS = 1000
const DEFAULT_NUMBER_OF_RETRIES = 3

module.exports = function (promiseMaker, options) {
  options = options || {}
  const timeoutMs = options.timeoutMs || DEFAULT_TIMEOUT_MS
  let count = options.count || DEFAULT_NUMBER_OF_RETRIES
  let onEachTry = options.onEachTry

  if (typeof onEachTry === 'string') {
    const message = onEachTry
    onEachTry = (triesLeft) => console.log(message.replace('{}', triesLeft))
  }

  return (function attempt () {
    return promiseMaker().catch(err => {
      if (onEachTry) onEachTry(count)
      if (--count <= 0) throw err
      return new Promise(resolve => setTimeout(() => resolve(attempt()), timeoutMs))
    })
  })()
}
