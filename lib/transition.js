'use strict'
const co = require('co')
const clientMaker = require('./retrying-promise-http-client')
const RETRIES = 3

function makeMatcher (config, client) {
  return co.wrap(function * (issue) {
    const transition = issue.transitions.find(t => t.name === config.transitionName)
    if (!transition) return null
    if (config.priority) {
      const prioritiesResponse = yield client.get(opts.priorities, opts.retries(RETRIES, 'failed to get priorities'))
      const priorityIds = JSON.parse(prioritiesResponse.body).map(pri => parseInt(pri.id, 10))
      const targetId = parseInt(config.priority.id, 10)
      const issueId = parseInt(issue.fields.priority.id, 10)
      return priorityIds.indexOf(issueId) <= priorityIds.indexOf(targetId) ? transition : null
    } else if (config.group) {
      const groupCheckFail = 'failed to get reporter group membership for issue "' + issue.key + '"'
      const reporterResponse = yield client.get(opts.reporter(issue.fields.reporter.name), opts.retries(RETRIES, groupCheckFail))
      return JSON.parse(reporterResponse.body).groups.items.some(group => group.name === config.group.name) ? transition : null
    } else return transition
  })
}

const executeMatchingTransition = co.wrap(function * (client, issueId, res, transitionMatcher) {
  const getIssueFail = 'failed to get issue ' + issueId
  const transitionFail = 'failed to transition issue ' + issueId

  try {
    const issueResponse = yield client.get(opts.issue(issueId), opts.retries(RETRIES, getIssueFail))
      .catch(() => [404, 'issue-not-found'])
    if (!issueResponse.body) return issueResponse

    const issue = JSON.parse(issueResponse.body)
    const transition = yield transitionMatcher(issue)
    if (transition == null) return [200, 'no-applicable-transition']

    yield client.post(opts.transitions(issueId, transition.id), opts.retries(RETRIES, transitionFail))
    return [200, 'success']
  } catch (e) {
    console.error(e.body || e.statusCode || e)
    return [500, 'transition-error']
  }
})

const opts = {
  issue: issueId => ({url: '/rest/api/2/issue/' + issueId + '?expand=transitions'}),
  reporter: username => ({url: '/rest/api/2/user?username=' + username + '&expand=groups'}),
  transitions: (issueId, transitionId) =>
    ({
      headers: {'Content-type': 'application/json'},
      json: {transition: {id: transitionId}},
      url: '/rest/api/2/issue/' + issueId + '/transitions'
    }),
  priorities: {url: '/rest/api/2/priority'},
  retries: (count, message) => ({retry: {onEachTry: message + ' - will try {} more times', count: count}})
}

module.exports = function postFunction (config, issueId, httpClient, res) {
  const client = clientMaker(httpClient)
  return executeMatchingTransition(client, issueId, res, makeMatcher(config, client))
}
