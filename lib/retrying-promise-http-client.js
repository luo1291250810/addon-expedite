'use strict'
const _ = require('lodash')
const withRetries = require('./promise-with-retries')
module.exports = function (httpClient) {
  const newClient = {}
  const methods = ['get', 'post', 'put', 'del', 'head', 'patch']
  methods.forEach(method => newClient[method] = toPromiseWithRetries(httpClient, method))
  return newClient
}

function toPromiseWithRetries (httpClient, method) {
  const promiseMaker = opts => new Promise((resolve, reject) => {
    httpClient[method](opts, (err, resp) => {
      if (err) reject({error: err, response: null})
      else if (resp.statusCode > 299) reject({error: err, response: resp})
      else resolve(resp)
    })
  })

  return (clientOpts, opts) => opts.retry ? withRetries(() => promiseMaker(_.extend({}, clientOpts)), opts.retry) : promiseMaker(clientOpts)
}
