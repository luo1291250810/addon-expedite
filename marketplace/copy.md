## Tagline

Get things moving

## Summary

Expedite for JIRA lets you re-transition issues based on group membership or issue priority - get issues raised by team members rubber-stamped, or get high-priority issues looked at right away.

## More details

Privacy and information security: 

This add-on looks up issue details, group membership and issue priority details, but doesn't store them.
All configuration is stored in the host JIRA.

Caveats:

Unfortunately some gaps in the public REST API mean that the name of the transition has to be typed in without auto-complete.
This plugin can't write to any fields or do anything else fancy during a transition, only trigger it.
If you're adding this to the 'Create' transition, you should put it last (or at least after the re-index / event step), to ensure the issue is ready to be transitioned when this expedite kicks in
It's possible to create a transition loop using this plugin. Be careful!

## Key features

### Expedite important issues

Re-transition an issue to any other state

### Seamless workflow integration

Expedite works wherever post functions work

### Decide what gets expedited

Based on group membership of the reporter, or priority of the issue

